import React from 'react';
import './intro.css'
import Picture from '../assest/girl.png'



function Intro() {
  return (

    <section id='intro'>

      <div className='introPart'>
        <div className='introPart00'>
          <h1 className='introHeading'>Overcome legal abstacles to your <span className='spanColor'>success!</span></h1>
          <p className='introPara'>Law without obstacles for your success. Professional support to help you overcome legal difficulties and achieve your goals.</p>
          <button className='introButton'>Book a free consultation</button>
        </div>
        <div className='introPart01'><img src={Picture} alt="introGirl" className='introImage'></img></div>
      </div>
    </section>
  )
}

export default Intro
