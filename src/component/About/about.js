import React from 'react'
import './about.css'
import Picture from '../assest/second.png'


function About() {
  return (
    <section id='aboutContainer'>
      <div className='aboutHead'>
        <div className='about00'>lenovo</div>
        <div className='about00'>lenovo</div>
        <div className='about00'>lenovo</div>
        <div className='about00'>lenovo</div>
        <div className='about00'>lenovo</div>
        <div className='about00'>lenovo</div>
        <div className='about00'>lenovo</div>
      </div>

      <div className='topBody'>
        <div className='aboutBody'>
          <p className='para1'>About us</p>
          <h1>A few words<br /> about our law firm</h1>
          <p>Trust, modernity, success. Our law firm builds relationships based on trust and security. We act quickly, ensuring satisfaction and comfort. we are your partner in business, removing legal obstacles to success</p>
          <button>More information about the law firm</button>

        </div>

        <div className='firstBox'>
          <div className='secondBox'><img src={Picture} alt="aboutImage" className='aboutImage'></img></div>
        </div>
      </div>
    </section>
  )
}

export default About
