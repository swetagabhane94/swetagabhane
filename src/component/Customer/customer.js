import React from 'react';
import './customer.css';
import { IoLogoTwitter } from 'react-icons/io';
import Image1 from '../assest/s1.png'
import Image2 from '../assest/s2.png'
import Image3 from '../assest/s3.png'
import Image4 from '../assest/s4.png'

function Customer() {
    return (
        <section className='customerContainer'>

            <p className='title1'>Opinions about us</p>
            <pre>
                {'What our customers say \nabout us'}
            </pre>

            <div className='customerGrid'>

                <div className="customerView">
                    <div className='customerName'>
                        <img src={Image1} alt=''></img>
                        <div className='name1'>
                            <h4>Jerome Bell</h4>
                            <p>@Jerome_Bell</p>
                        </div>
                        <div className='logo'><IoLogoTwitter /></div>

                    </div>
                    <p className='customerPara'>Lorem ipsum dolor sit amet consectetur. Parturient leo aliquet ligula enim lacinia vitae massa convallis felis. Ante eu et viverra suscipit in. Nam a integer libero lorem eget tempor pharetra. Lectus etiam consequat at vitae mauris.</p>
                </div>


                <div className="customerView">
                    <div className='customerName'>
                        <img src={Image2} alt=''></img>
                        <div className='name1'>
                            <h4>Jerome Bell</h4>
                            <p>@Jerome_Bell</p>
                        </div>
                        <div className='logo'><IoLogoTwitter /></div>

                    </div>
                    <p className='customerPara'>Lorem ipsum dolor sit amet consectetur. Parturient leo aliquet ligula enim lacinia vitae massa convallis felis. Ante eu et viverra suscipit in. Nam a integer libero lorem eget tempor pharetra. Lectus etiam consequat at vitae mauris.</p>
                </div>


                <div className="customerView">
                    <div className='customerName'>
                        <img src={Image3} alt=''></img>
                        <div className='name1'>
                            <h4>Jerome Bell</h4>
                            <p>@Jerome_Bell</p>
                        </div>
                        <div className='logo'><IoLogoTwitter /></div>

                    </div>
                    <p className='customerPara'>Lorem ipsum dolor sit amet consectetur. Parturient leo aliquet ligula enim lacinia vitae massa convallis felis. Ante eu et viverra suscipit in. Nam a integer libero lorem eget tempor pharetra. Lectus etiam consequat at vitae mauris.</p>
                </div>


                <div className="customerView">
                    <div className='customerName'>
                        <img src={Image4} alt=''></img>
                        <div className='name1'>
                            <h4>Jerome Bell</h4>
                            <p>@Jerome_Bell</p>
                        </div>
                        <div className='logo'><IoLogoTwitter /></div>

                    </div>
                    <p className='customerPara'>Lorem ipsum dolor sit amet consectetur. Parturient leo aliquet ligula enim lacinia vitae massa convallis felis. Ante eu et viverra suscipit in. Nam a integer libero lorem eget tempor pharetra. Lectus etiam consequat at vitae mauris.</p>
                </div>



                <div className="customerView">
                    <div className='customerName'>
                        <img src={Image3} alt=''></img>
                        <div className='name1'>
                            <h4>Jerome Bell</h4>
                            <p>@Jerome_Bell</p>
                        </div>
                        <div className='logo'><IoLogoTwitter /></div>

                    </div>
                    <p className='customerPara'>Lorem ipsum dolor sit amet consectetur. Parturient leo aliquet ligula enim lacinia vitae massa convallis felis. Ante eu et viverra suscipit in. Nam a integer libero lorem eget tempor pharetra. Lectus etiam consequat at vitae mauris.</p>
                </div>



                <div className="customerView">
                    <div className='customerName'>
                        <img src={Image4} alt=''></img>
                        <div className='name1'>
                            <h4>Jerome Bell</h4>
                            <p>@Jerome_Bell</p>
                        </div>
                        <div className='logo'><IoLogoTwitter /></div>

                    </div>
                    <p className='customerPara'>Lorem ipsum dolor sit amet consectetur. Parturient leo aliquet ligula enim lacinia vitae massa convallis felis. Ante eu et viverra suscipit in. Nam a integer libero lorem eget tempor pharetra. Lectus etiam consequat at vitae mauris.</p>
                </div>




            </div>

            <div className='da1'> <button className='customerBtn'>Read more obout the opinions</button>

            </div>
        </section>

    )
}

export default Customer;
