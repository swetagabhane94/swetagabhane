import React, { useState } from 'react';
import './header.css';
import { Link } from 'react-scroll'
import {GiHamburgerMenu} from 'react-icons/gi'

const Header = () => {

  const [showMenu,setShowMenu]=useState (false)
  return (
    <nav className='navbar'>

      <div className='headerData'>
        <p className='textStyle'><b>snazzy</b>lawyer</p>

        <div className='desktopMenu'>
          <Link className='desktopMenuListItem'>About us</Link>
          <Link className='desktopMenuListItem'>Offer</Link>
          <Link className='desktopMenuListItem'>Our Team</Link>
          <Link className='desktopMenuListItem'>Blog</Link>
          <Link className='desktopMenuListItem'>Contact</Link>
        </div>

        <button className='desktopMenuButton'>Make an appointment</button>
     

      <GiHamburgerMenu className='mobileData' onClick={()=>setShowMenu(!showMenu)} />
      <div className='mobileMenu' style={{display:showMenu?'flex':'none'}}>
          <Link className='mobileList' onClick={()=>setShowMenu(false)}>About us</Link>
          <Link className='mobileList'  onClick={()=>setShowMenu(false)}>Offer</Link>
          <Link className='mobileList'  onClick={()=>setShowMenu(false)}>Our Team</Link>
          <Link className='mobileList'  onClick={()=>setShowMenu(false)}>Blog</Link>
          <Link className='mobileList'  onClick={()=>setShowMenu(false)}>Contact</Link>
        </div>
        </div>
    </nav>
  )
}

export default Header
