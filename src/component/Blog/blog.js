import React from 'react'
import './blog.css'
import Blog1 from '../assest/blog1.png'
import Blog2 from '../assest/blog2.png'
import Blog3 from '../assest/blog3.png'


function Blog() {
  return (
    <section className="latest-article">
      <p className='title1'>Blog</p>
      <h1>Newsroom</h1>
      <div className="articles">


        <div className="articles-box">
          <div className="articles-top">
            <img src={Blog1} alt="" className="article-0" />
          </div>
          <div className="articles-bottom">
            <span className="postBy">Criminal law</span>
            <h4 className="article-title">How do you prepare for a company’s RODO audit and implementation?</h4>
            <div className="title-explain">
              <span>Lorem ipsum dolor sit amet consectetur. Parturient leo aliquet ligula enim lacinia .</span>
            </div>
          </div>
        </div>

        <div className="articles-box">
          <div className="articles-top">
            <img src={Blog2} alt="" className="article-0" />
          </div>
          <div className="articles-bottom">
            <span className="postBy">Criminal law</span>
            <h4 className="article-title">How to successfully recover debts from an unreliable debtor?</h4>
            <div className="title-explain">
              <span>Lorem ipsum dolor sit amet consectetur. Parturient leo aliquet ligula enim lacinia .</span>
            </div>
          </div>
        </div>

        <div className="articles-box">
          <div className="articles-top">
            <img src={Blog3} alt="" className="article-0" />
          </div>
          <div className="articles-bottom">
            <span className="postBy">Criminal law</span>
            <h4 className="article-title">Division of property
              during divorce</h4>
            <div className="title-explain">
              <span>Lorem ipsum dolor sit amet consectetur. Parturient leo aliquet ligula enim lacinia .</span>
            </div>
          </div>
        </div>
      </div>
    </section>


  )
}

export default Blog
