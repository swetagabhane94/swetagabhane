import React from 'react'
import './our.css'
import { VscLaw } from 'react-icons/vsc';
import { TbJewishStarFilled, TbDiscountCheckFilled, TbArrowUpRight } from 'react-icons/tb'
import { RiMoneyDollarBoxFill } from 'react-icons/ri'
import { GiThorHammer } from 'react-icons/gi'
import { PiSuitcaseSimpleBold } from 'react-icons/pi'
import { MdOutlineRealEstateAgent } from 'react-icons/md'
import { LuPiggyBank } from 'react-icons/lu'

function our() {
  return (
    <section className='ourContainer'>

      <p className='data01'>Speciallsation</p>
      <h1>What we do</h1>
      <pre className='data02'>
        {'Our lawyer are specialists in criminal law, civil law, commercial law, administrative law and \nnew technologies. We are not afraid of chanllenges, including consumer bankruptcy'}
      </pre>


      <div className="ourGrid">

        <div className="item">
          <div className='item-1'><VscLaw /></div>
          <div className='item-2'>
            <p>Law Family</p>
            <div><TbArrowUpRight /></div>
          </div>
        </div>

        <div className="item">
          <div className='item-1'><GiThorHammer /></div>
          <div className='item-2'>
            <p>Law Crimial</p>
            <div><TbArrowUpRight /></div>
          </div>
        </div>

        <div className="item">
          <div className='item-1'><RiMoneyDollarBoxFill /></div>
          <div className='item-2'>
            <p>Law Civil</p>
            <div><TbArrowUpRight /></div>
          </div>
        </div>

        <div className="item">
          <div className='item-1'><PiSuitcaseSimpleBold /></div>
          <div className='item-2'>
            <p>Law Employment</p>
            <div><TbArrowUpRight /></div>
          </div>
        </div>


        <div className="item">
          <div className='item-1'><MdOutlineRealEstateAgent /></div>
          <div className='item-2'>
            <p>Law Realestate</p>
            <div><TbArrowUpRight /></div>
          </div>
        </div>

        <div className="item">
          <div className='item-1'><TbJewishStarFilled /></div>
          <div className='item-2'>
            <p className='comp'>Compensation</p>
            <div className='comp1'><TbArrowUpRight /></div>
          </div>
        </div>

        <div className="item">
          <div className='item-1'><TbDiscountCheckFilled /></div>
          <div className='item-2'>
            <p>Cases Successions</p>
            <div><TbArrowUpRight /></div>
          </div>
        </div>

        <div className="item">
          <div className='item-1'><LuPiggyBank /></div>
          <div className='item-2'>
            <p>Bankruptcy Consumer</p>
            <div><TbArrowUpRight /></div>
          </div>
        </div>
      </div>

    </section>
  )
}

export default our
