import React from 'react'
import './footer.css'
import { FaTiktok } from "react-icons/fa"
import { FaSquareFacebook } from 'react-icons/fa6'
import { LiaYoutubeSquare } from 'react-icons/lia'

function Footer() {
  return (
    <footer id="footerContainer">

      <div className='footerData'>

        <div>
          <ul className="nav-0">
            <li><b>snazzy</b>lawyer</li>
            <li className='list'>help@snazzylawyer.com</li>
            <li className='list'>+48 601 908 812</li>

          </ul>
        </div>

        <div className='foot1'>
          <ul className="nav-1">
            <li>Specialties</li>
            <li>Family law</li>
            <li>Criminal law</li>
            <li>Civil law</li>
            <li>Labour law</li>
            <li>Real estate law</li>
          </ul>

          <ul className="nav-2">
            <li>Compensation</li>
            <li>Succession matters</li>
            <li>Consumer bankruptcy</li>
            <li>Drafting and reviewing contracts</li>
          </ul>

          <ul className="nav-3">
            <li>Resources</li>
            <li>FQA</li>
            <li>Blog</li>
            <li>Kontakt</li>
          </ul>
        </div>
      </div>

      <hr />
      <div className="footerEnd">
        <div className='d0'>©SnazzylawyerAll Right Reserved</div>
        <div className='data1'>
          <div className='s'><LiaYoutubeSquare /></div>
          <div className='s'> <FaSquareFacebook /></div>
          <div className='s'> <FaTiktok /></div>



        </div>
        <div className='d1'>Privacy policy</div>
      </div>

    </footer>

  )
}

export default Footer
