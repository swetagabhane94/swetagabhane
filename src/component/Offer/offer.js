import React from 'react';
import './offer.css'

function Offer() {
  return (
    <section className='offerContainer'>


      <p className='offerPara'>What makes us different</p>
      <h1 className='offerHeading'>Why choose our law firm?</h1>
      <div className='offerBox'>
        <div className='offerBox1'>
          <h1>95%</h1>
          <p>Thanks to our skills and commitment, our law firm achieves a high degree of efficiency in resolving cases.</p>
          <button>Book a free consulation</button>
        </div>
        <div className='verticalLine'></div>
        <div className='offerBox2'>
          <h1>10+</h1>
          <p>Years of experience is the solid foundation on which we base our law firm.</p>
          <div className='hdd'></div>
          <h1>+ 500</h1>
          <p>More than 500 satisfied clients are the result of our excellent legal services</p>
        </div>
      </div>

    </section>
  )
}

export default Offer
