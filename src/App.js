import Header from "./component/Header/header";
import Intro from "./component/Intro/intro";
import About from "./component/About/about";
import  Offer from "./component/Offer/offer"
import Our from "./component/Our/our"
import Customer from "./component/Customer/customer";
import Blog from "./component/Blog/blog";
import Footer from "./component/Footer/footer"

function App() {
  return (
    <div className="App">
      
      <Header />
      <Intro />
      <About />
      <Offer />
      <Our />
      <Customer />
      <Blog />
      <Footer />
    </div>
  );
}

export default App;
